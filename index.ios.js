import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import RootScreen from './src/screens';

AppRegistry.registerComponent('cobak', () => RootScreen);
