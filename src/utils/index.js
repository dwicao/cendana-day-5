import {
  Platform,
  Dimensions,
} from 'react-native';

export const diff = (Platform.OS === 'ios') ? 20 : 0;
export const width = Dimensions.get('window').width;
export const height = Dimensions.get('window').height;
