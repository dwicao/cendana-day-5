import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Animated,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import imgLoading from '../../images/loading.gif';
import { width, height, totalSize } from 'react-native-dimension';
import * as warna from '../../colors';

class MyButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };

    this.tombolAnimasi = new Animated.Value(0);
    this.manipulasiOpacity = new Animated.Value(0);
    this.onPencet = this.onPencet.bind(this);
  }

  onPencet() {
    Animated.timing(       // Uses easing functions
      this.tombolAnimasi, // The value to drive
      {
        toValue: 100,        // Target
        duration: 2000,    // Configuration
      },
    ).start(() => this.tombolAnimasi.setValue(0));             // Don't forget start!

    Animated.timing(       // Uses easing functions
      this.manipulasiOpacity, // The value to drive
      {
        toValue: 1,        // Target
        duration: 2000,    // Configuration
      },
    ).start();

    this.setState({
      isLoading: true,
    });

    setTimeout(() => {
      this.setState({ isLoading: false });
      Actions.home();
    }, 2000);
  }

  render() {
    const ubahWidth = this.tombolAnimasi.interpolate({
      inputRange: [0, 100],
      outputRange: [width(90), width(15)],
    });

    const ubahOpacity = this.manipulasiOpacity.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [1, 0, 1],
    });

    return (
      <Animated.View style={{ width: ubahWidth, opacity: ubahOpacity }}>
        <TouchableOpacity
          onPress={this.onPencet}
          style={styles.tombolKu}
        >
          {
            this.state.isLoading ? 
              <Image source={imgLoading} style={styles.loadingGif}/>
            :
              <Text>Login</Text>
          }
        </TouchableOpacity>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  tombolKu: {
    alignItems: 'center',
    justifyContent: 'center',
    height: height(8),
    borderRadius: height(4),
    backgroundColor: warna.PRIMARY,
  },
  loadingGif: {
    width: height(7.5),
    height: height(7.5),
  },
});

export default MyButton;
