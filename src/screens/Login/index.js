import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
} from 'react-native';
import imgLogo from '../../images/logo.png';
import MyTextInput from './MyTextInput';
import MyButton from './MyButton';
import { diff } from '../../utils';
import * as warna from '../../colors';

class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={imgLogo} style={styles.logo}/>
        <MyTextInput placeholderSaya="Username" />
        <MyTextInput placeholderSaya="Password" />
        <Text>hai</Text>
        <MyButton />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: diff,
  },
  logo: {
    width: 50,
    height: 50,
    tintColor: warna.PRIMARY,
  },
});

export default Login;