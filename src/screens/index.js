import React from 'react';
import {Scene, Router} from 'react-native-router-flux';
import Login from './Login';
import Home from './Home';

class RootScreen extends React.Component {
  render() {
    return ( 
      <Router>
        <Scene key="root" hideNavBar>
          <Scene key="home" component={Home}/>
          <Scene key="login" component={Login} initial/>
        </Scene>
      </Router>
    )
  }
}

export default RootScreen;
