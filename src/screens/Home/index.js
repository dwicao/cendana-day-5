import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import * as colors from '@colors';
import { Actions } from 'react-native-router-flux';
import TopBar from '@components/TopBar';

class Home extends Component {
  constructor(props) {
    super(props);

    this.onPencetBack = this.onPencetBack.bind(this);
    this.onPencetMenu = this.onPencetMenu.bind(this);
  }

  onPencetBack() {
    Actions.pop();
  }

  onPencetMenu() {
    alert('menuuu');
  }

  render() {
    return (
      <View style={styles.container}>
        <TopBar judul="Home" />
        <Text>Ini adalah contentnya Home</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
  },
});

export default Home;
