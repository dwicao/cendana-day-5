import React, { Component, PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { diff } from '@utils';
import * as colors from '@colors';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';

class TopBar extends Component {
  constructor(props) {
    super(props);

    this.onPencetBack = this.onPencetBack.bind(this);
    this.onPencetMenu = this.onPencetMenu.bind(this);
  }

  onPencetBack() {
    Actions.pop();
  }

  onPencetMenu() {
    alert('menuuu');
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.onPencetBack}>
          <Icon name="md-arrow-round-back" size={30} color="#000" />
        </TouchableOpacity>
        <Text>{this.props.judul}</Text>
        <TouchableOpacity onPress={this.onPencetMenu}>
          <Icon name="md-menu" size={30} color="#000" />
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: diff,
    height: 50,
    backgroundColor: colors.PRIMARY,
  },
});

TopBar.propTypes = {
  judul: PropTypes.string.isRequired,
}

export default TopBar;
